#include <Arduino.h>
#include <ESPert.h>

ESPert espert;

/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the Uno and
  Leonardo, it is attached to digital pin 13. If you're unsure what
  pin the on-board LED is connected to on your Arduino model, check
  the documentation at http://www.arduino.cc

  This example code is in the public domain.

  modified 8 May 2014
  by Scott Fitzgerald
 */


// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin 13 as an output.
  pinMode(14, OUTPUT);
  Serial.begin(115200);
  espert.init();
  espert.oled.init();
  delay(2000);
  espert.oled.clear();
  espert.oled.setTextSize(1);
  espert.oled.setTextColor(ESPERT_WHITE);
  espert.oled.setCursor(0, 0);
  espert.oled.println("Hello");
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(14, HIGH);   // turn the LED on (HIGH is the voltage level)
  Serial.println("ON");
  delay(4000);              // wait for a second
  digitalWrite(14, LOW);    // turn the LED off by making the voltage LOW
  Serial.println("OFF");
  delay(1000);              // wait for a second
}
