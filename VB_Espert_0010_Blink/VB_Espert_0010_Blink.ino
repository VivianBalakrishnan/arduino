/* 
 * EPresso 1.0 LED_BUILTIN = 16;
 * EPresso 2.0 LED_BUILTIN = 2;
 */

#include <ESPert.h>

ESPert espert;

void setup() {
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(14, OUTPUT);

  WiFi.begin("vbhome","2233445566");

  espert.init();
  Serial.println(espert.wifi.getLocalIP());
}

void loop() {
  digitalWrite(LED_BUILTIN, LOW);
  digitalWrite(14, LOW);
  Serial.println("LED: On");
  delay(1000);

  digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(14, HIGH);
  Serial.println("LED: Off");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  delay(1000);
}
